import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.click = this.click.bind(this)
    this.state = {
      list: [],
      currentPokemon: null
    }
  }

  componentDidMount() {
    // Nous récupérons la liste de 100 pokemon au démarage de l'application
    fetch('https://pokeapi.co/api/v2/pokemon?limit=151')
      .then(res => res.json())
      .then(json => {
        console.log(json)
        this.setState({
          list: json.results
        })
      })
  }

  click(url) {
    // url corresponds aux informations que nous devons récupérer sur le pokemon selectionner
    fetch(url)
      .then(res => res.json())
      .then(json => {
        console.log(json)
        // nous stokons dans currentPokemon les informations du pokemon selectionner
        this.setState({
          currentPokemon: json
        })
      })
  }
  render() {
    return (
      <div className="App">
        <div className='sticky'>
          <section className="single-poke">
          <img src="https://logos-world.net/wp-content/uploads/2020/05/Pokemon-Logo.png" alt="logo-pokemon"/>

            {this.state.currentPokemon ?
              (<div className="poke-info">
                <img className="big" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.state.currentPokemon.id}.png`} alt="img-info"></img>
                <div>
                  <p><b>Name : </b> {this.state.currentPokemon.name}</p>
                  <p><b>Height : </b>{this.state.currentPokemon.height}</p>
                  <p><b>Weight : </b>{this.state.currentPokemon.weight}</p>
                  <p><b>Type : </b>{this.state.currentPokemon.types[0].type.name}</p>
                </div>
              </div>) :
              (<h1>Choisis un pokemon</h1>)
            }
          </section>
        </div>
        <section className="poke-img">
          {this.state.list.map((elem, index) => {
            return (
              <button onClick={() => this.click(elem.url)}>
                <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`} alt="pokemon"></img>
                <p>{elem.name}</p>
              </button>
            )
          })}
        </section>
      </div>
    );
  }
}
export default App;